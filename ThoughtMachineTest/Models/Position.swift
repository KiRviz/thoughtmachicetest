//
//  Position.swift
//  ThoughtMachineTest
//
//  Created by Darius Jankauskas on 06/12/2019.
//  Copyright © 2019 Darius Jankauskas. All rights reserved.
//

import Foundation

struct PositionResponse: Codable {
    let iss_position: Position
}

struct Position: Codable {
    let latitude: String
    let longitude: String
}
