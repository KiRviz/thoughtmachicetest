//
//  Flyover.swift
//  ThoughtMachineTest
//
//  Created by Darius Jankauskas on 05/12/2019.
//  Copyright © 2019 Darius Jankauskas. All rights reserved.
//

import Foundation

struct FlyoverResponse: Codable {
    let response: [Flyover]
}

struct Flyover: Codable {
    let risetime: Date
}
