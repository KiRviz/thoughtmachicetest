//
//  MapView.swift
//  ThoughtMachineTest
//
//  Created by Darius Jankauskas on 06/12/2019.
//  Copyright © 2019 Darius Jankauskas. All rights reserved.
//

import SwiftUI
import MapKit

extension Position {
    func toLocation() -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: Double(latitude) ?? 0, longitude: Double(longitude) ?? 0)
    }
}

class LandmarkAnnotation: NSObject, MKAnnotation {
    let title: String?
    let subtitle: String?
    let coordinate: CLLocationCoordinate2D
    
    init(title: String? = "ISS",
         subtitle: String? = "International Space Station",
         coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
    }
}

class MapViewModel: ObservableObject {
    @Published var landmarks: [LandmarkAnnotation] = []
    
    private var tracking = false
    
    private let client: RestClient
    
    init(client: RestClient) {
        self.client = client
    }
    
    func startTracking() {
        tracking = true
        fetchPosition()
    }
    
    func stopTracking() {
        tracking = false
    }
    
    private func fetchPosition() {
        guard tracking else { return }
        
        client.fetchCurrentPosition { [weak self] result in
            switch result {
            case .success(let position):
                self?.landmarks = [LandmarkAnnotation(coordinate: position.toLocation())]
            case .failure(let error):
                print(error)
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.fetchPosition()
        }
        
        print("fetch Position") // leaving this here to prove fetching stops when supposed to stop
    }
}

class MapViewCoordinator: NSObject, MKMapViewDelegate {
    var mapViewController: MapKitWrapperView
    
    init(_ control: MapKitWrapperView) {
        self.mapViewController = control
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "customView")
        annotationView.canShowCallout = true
        annotationView.image = UIImage(named: "ISS_icon")
        return annotationView
    }
}

struct MapKitWrapperView: UIViewRepresentable {
    @EnvironmentObject var viewModel: MapViewModel
    
    let landmarks: [LandmarkAnnotation]
    
    func makeUIView(context: Context) -> MKMapView {
        return MKMapView(frame: .zero)
    }
    
    func makeCoordinator() -> MapViewCoordinator {
        MapViewCoordinator(self)
    }
    
    func updateUIView(_ view: MKMapView, context: Context){
        view.delegate = context.coordinator
        
        view.removeAnnotations(view.annotations)
        view.addAnnotations(landmarks)
        
        // scroll to ISS
        if let position = landmarks.first?.coordinate {
            let region = MKCoordinateRegion(center: position, span: view.region.span)
            view.setRegion(region, animated: true)
        }
    }
}

struct MapView: View {
    @EnvironmentObject var viewModel: MapViewModel
    
    var body: some View {
        ZStack {
            Color.spaceBlack.edgesIgnoringSafeArea(.all)
            MapKitWrapperView(landmarks: viewModel.landmarks)
        }.navigationBarTitle(Text("Position"))
        .onAppear() {
            self.viewModel.startTracking()
        }.onDisappear() {
            self.viewModel.stopTracking()
        }
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
}
