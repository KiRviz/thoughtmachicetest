//
//  ContentView.swift
//  ThoughtMachineTest
//
//  Created by Darius Jankauskas on 05/12/2019.
//  Copyright © 2019 Darius Jankauskas. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var dates = [Date]()

    var body: some View {
        NavigationView {
            MasterView()
            .navigationBarTitle("Mission Control")
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}

struct MasterView: View {
    var body: some View {
        Color.spaceBlack.edgesIgnoringSafeArea(.all).overlay(
            VStack(spacing: 20) {
                Text("When is the ISS flying above me?")
                    .styleAsSpaceSubTitle()
                NavigationLink(destination: FlyoversView()) {
                    Text("Check out the flyovers")
                }.buttonStyle(SpaceButton())
                Text("")
                Text("Where's the ISS right now?")
                    .styleAsSpaceSubTitle()
                NavigationLink(destination: MapView()) {
                    Text("Check out the map")
                }.buttonStyle(SpaceButton())
            }.padding([.leading, .trailing], 10)
                
        )
    }
}

extension Text {
    func styleAsSpaceSubTitle() -> some View {
        self.font(.system(size: 16, weight: .light))
            .foregroundColor(Color.spaceGreen)
            .multilineTextAlignment(.center)
    }
}

struct SpaceButton: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .font(.system(size: 20, weight: .semibold))
            .frame(maxWidth: .infinity)
            .padding()
            .foregroundColor(configuration.isPressed ? Color.spaceBlack : Color.spaceGreen)
            .background(configuration.isPressed ? Color.spaceDarkGreen : Color.spaceBlack)
            .cornerRadius(10)
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color.spaceDarkGreen))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
