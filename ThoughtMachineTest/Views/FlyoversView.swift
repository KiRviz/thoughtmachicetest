//
//  FlyoversView.swift
//  ThoughtMachineTest
//
//  Created by Darius Jankauskas on 05/12/2019.
//  Copyright © 2019 Darius Jankauskas. All rights reserved.
//

import SwiftUI

struct FlyoversView: View {
    @EnvironmentObject var viewModel: FlyoversViewModel
    
    var body: some View {
        ZStack {
            Color.spaceBlack.edgesIgnoringSafeArea(.all)
            if viewModel.flyoverRows.count == 0 {
                ActivityIndicator(isAnimating: .constant(true), style: .large, color: UIColor.spaceGreen)
            }
            ScrollView {
                VStack(spacing: 10) {
                    ForEach(viewModel.flyoverRows, id: \.date) { (flyover: FlyoverRowViewModel) in
                        FlyoverRow(model: flyover)
                    }
                }.frame(maxWidth: .infinity)
            }.padding([.leading, .trailing, .top], 10)
        }.onAppear() {
            self.viewModel.fetch()
        }.navigationBarTitle(Text("Flyover"))
    }
}

struct FlyoversView_Previews: PreviewProvider {
    static var previews: some View {
        let response: FlyoverResponse = loadJSON(fromFile: "flyovers.json")
        let flyovers = response.response
        let viewModel = FlyoversViewModel(flyovers: flyovers, client: nil)
        
        return NavigationView {
            FlyoversView().environmentObject(viewModel)
        }
    }
}
