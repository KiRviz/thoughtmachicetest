//
//  FlyoverRow.swift
//  ThoughtMachineTest
//
//  Created by Darius Jankauskas on 05/12/2019.
//  Copyright © 2019 Darius Jankauskas. All rights reserved.
//

import SwiftUI

struct FlyoverRow: View {
    private let model: FlyoverRowViewModel
    
    init(model: FlyoverRowViewModel) {
        self.model = model
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(model.shortDate)
                .font(.system(size: 18))
            Text(model.time)
                .font(.system(size: 52))
        }.modifier(SpaceTime())
        .padding(1)
    }
}

struct SpaceTime: ViewModifier {
    func body(content: Content) -> some View {
        content
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(8)
            .foregroundColor(Color.spaceGreen)
            .background(Color.spaceBlack)
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color.spaceDarkGreen))
    }
}

struct FlyoverRowViewModel {
    let shortDate: String
    let time: String
    let date: Date

    init(model: Flyover) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE d MMM yyyy"
        shortDate = dateFormatter.string(from: model.risetime)
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        time = timeFormatter.string(from: model.risetime)
        
        date = model.risetime
    }
    
    init(error: Error) {
        shortDate = "Sorry there was an error, please tap back and try again or send us the screenshot with the error below:\n\n\(error)"
        time = ""
        date = Date()
    }
}

struct FlyoverRow_Previews: PreviewProvider {
    static var previews: some View {
        let response: FlyoverResponse = loadJSON(fromFile: "flyovers.json")
        let flyovers = response.response
        let flyoverViewModel = FlyoverRowViewModel(model: flyovers[0])
        
        let errorViewModel = FlyoverRowViewModel(error: HTTPError.statusCode(666))
        
        return Group {
            FlyoverRow(model: flyoverViewModel)
            FlyoverRow(model: errorViewModel)
        }.previewLayout(.fixed(width: 400, height: 120))
    }
}
