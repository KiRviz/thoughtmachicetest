//
//  Colors.swift
//  ThoughtMachineTest
//
//  Created by Darius Jankauskas on 05/12/2019.
//  Copyright © 2019 Darius Jankauskas. All rights reserved.
//

import SwiftUI

extension UIColor {
    static let spaceGreen = UIColor(red: 0, green: 1, blue: 0, alpha: 1)
    static let spaceDarkGreen = UIColor(red: 0, green: 0.5, blue: 0, alpha: 1)
    static let spaceBlack = UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1)

}

extension Color {
    static let spaceGreen = Color(UIColor.spaceGreen)
    static let spaceDarkGreen = Color(UIColor.spaceDarkGreen)
    static let spaceBlack = Color(UIColor.spaceBlack)
}
