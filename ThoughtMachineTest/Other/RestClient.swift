//
//  Rest.swift
//  ThoughtMachineTest
//
//  Created by Darius Jankauskas on 05/12/2019.
//  Copyright © 2019 Darius Jankauskas. All rights reserved.
//

import Combine
import SwiftUI

enum HTTPError: LocalizedError {
    case statusCode(Int)
}

class RestClient {
    private let baseURL = "http://api.open-notify.org"
    
    private var disposables = Set<AnyCancellable>()
    
    // The following two functions could be extracted into separate one-function classes
    // Functions fetch() and generateURL() would then be public of course
    
    public func fetchCurrentPosition(completion: @escaping (Result<Position, Error>) -> ()) {
        guard let url = generateURL(with: .currentPosition) else { return }
        
        fetch(url: url) { (result: Result<PositionResponse, Error>) in
            completion(result.map {$0.iss_position} )
        }
    }
    
    public func fetchFlyovers(latitude: Double, longitude: Double, limit: Int=5, completion: @escaping (Result<[Flyover], Error>) -> ()) {
        guard let url = generateURL(with: .flyover(latitude: latitude, longitude: longitude, limit: limit)) else { return }
        
        fetch(url: url) { (result: Result<FlyoverResponse, Error>) in
            completion(result.map { $0.response })
        }
    }
    
    private func fetch<T: Decodable>(url: URL, completion: @escaping (Result<T, Error>) -> ()) {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .secondsSince1970
        
        URLSession.shared.dataTaskPublisher(for: url)
            .tryMap { output in
                guard let response = output.response as? HTTPURLResponse, response.statusCode == 200 else {
                    let code = (output.response as? HTTPURLResponse)?.statusCode ?? 0
                    throw HTTPError.statusCode(code)
                }
                return output.data
            }
            .decode(type: T.self, decoder: decoder)
            .map { Result<T, Error>.success($0) }
            .catch({ error in
                return Just(Result<T, Error>.failure(error))
            })
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { value in
                completion(value)
            }).store(in: &disposables)
    }
    
    private func generateURL(with endpoint: Endpoint) -> URL? {
        let (path, queryItems) = endpoint.properties
        
        let urlString = "\(baseURL)\(path)"
        
        guard var urlComponents = URLComponents(string: urlString) else {
            return nil
        }
        
        urlComponents.queryItems = queryItems
        return urlComponents.url
    }
}

enum Endpoint {
    case flyover(latitude: Double, longitude: Double, limit: Int=5)
    case currentPosition
    
    var properties: (path: String, queryItems: [URLQueryItem]?) {
        switch self {
        case let .flyover(latitude, longitude, limit):
            return (path: "/iss-pass.json",
                    queryItems: [URLQueryItem(name: "lat", value: "\(latitude)"),
                                 URLQueryItem(name: "lon", value: "\(longitude)"),
                                 URLQueryItem(name: "n", value: "\(limit)")
                                ])
        case .currentPosition:
            return (path: "/iss-now.json", queryItems: nil)
        }
    }
}
