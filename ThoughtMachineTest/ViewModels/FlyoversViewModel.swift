//
//  FlyoversViewModel.swift
//  ThoughtMachineTest
//
//  Created by Darius Jankauskas on 06/12/2019.
//  Copyright © 2019 Darius Jankauskas. All rights reserved.
//

import Combine
import MapKit
import CoreLocation

class FlyoversViewModel: NSObject, CLLocationManagerDelegate, ObservableObject {
    @Published var flyoverRows: [FlyoverRowViewModel] = []
    private let locationManager = CLLocationManager()
    private var location = CLLocationCoordinate2D()
    
    private let client: RestClient?
    
    init(client: RestClient) {
        self.client = client
    }
    
    init(flyovers: [Flyover], client: RestClient?) {
        self.flyoverRows = flyovers.map { FlyoverRowViewModel(model: $0) }
        self.client = client
    }
    

    
    func fetch() {
        setupLocationServices()
    }
    
    private func fetchFlyovers(latitude: Double, longitude: Double) {
        client?.fetchFlyovers(latitude: latitude, longitude: longitude) { [weak self] result in
            switch result {
            case .success(let flyovers):
                print("success")
                self?.flyoverRows = flyovers.map { FlyoverRowViewModel(model: $0) }
            case .failure(let error):
                self?.flyoverRows = [FlyoverRowViewModel(error: error)]
                print(error)
            }
        }

    }
    
    private func setupLocationServices() {
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        #if targetEnvironment(simulator)
        print("locations = \(location.latitude) \(location.longitude)")
        #endif

        if location != self.location {
            fetchFlyovers(latitude: location.latitude, longitude: location.longitude)
        }
        
        self.location = location
    }
}


extension CLLocationCoordinate2D: Equatable {
  public static func == (left: CLLocationCoordinate2D, right: CLLocationCoordinate2D) -> Bool {
    let eps = 0.0001
    return abs(left.latitude - right.latitude) < eps && abs(left.longitude - right.longitude) < eps
  }
}
